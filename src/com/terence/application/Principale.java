// Nom du package où est définie la méthode main
package com.terence.application;

//Importation de la classe Personne avec son package d’appartenance
import com.terence.entites.Personne;

/**
 * @author User-05
 *
 */
public class Principale {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Passage dans main");
//		Personne martin = new Personne ( "martin", "Java SARL");
//		Personne dupont = new Personne ("dupont") ;
//		martin.afficher();
//		Personne durand = new Personne ("durand", "J2E SA");
//		durand.afficher();
//		durand.quitterSociete();
//		durand.afficher();
//		durand.setSociete("EJB Corporate");
//		durand.afficher();
//		dupont.quitterSociete();
		Personne martin = new Personne ("martin");
		martin.afficher();
		Personne durand = new Personne ("durand", "J2E SA");
		durand.afficher() ;
		durand.quitterSociete();
		durand.afficher() ;
		/**
		 * 19-20 à voir
		 */
		durand.setSociete("EJB Corporate");
		durand.afficher() ;
//		durand.setSociete("EJB Corporate");
//		durand.afficher() ;
	}

}
