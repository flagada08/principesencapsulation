package com.terence.entites;

/**
 * @author User-05
 *
 */
public class Personne {
	/**
	 * 
	 */
	private String nom, societe/**, tempSociete*/;
	// Variable de classe matérialisant le non rattachement à une société selon notre convention.
	/**
	 * 
	 */
	private static final String PAS_DE_SOCIETE = "?";
	// Capture de la data
	/**
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	// Traitement de la data
	/**
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return
	 */
	public String getSociete() {
		return societe;
	}

	/**
	 * @param societe
	 */
	public void setSociete(String societe) {
//		tempSociete = societe;
		this.societe = societe;		
		societe = validerSociete(societe).toUpperCase();
		// TODO FAIL à voir
		if(this.societe.equals(this.societe)) {
			System.out.println(getClass() + "\nErreur: " + societe + "\n1-quitterSociete, puis\n2-affecterSociete");
			System.exit(1);
		}
	}

	/**
	 * @param nom
	 */
	public Personne(String nom) {
		this.nom = nom;
	}

	/**
	 * @param nom
	 * @param societe
	 */
	public Personne(String nom, String societe) {
		this.nom = nom;
		this.societe = societe;
		societe = validerSociete(societe).toUpperCase();
	}

	/**
	 * 
	 */
	public void afficher() {
		if(getSociete() != null && societe != PAS_DE_SOCIETE) {			
			System.out.println("Je m'appelle " 
					+ this.getNom().toUpperCase() 
					+ " et je travaille chez " 
					+ getSociete());
		} else {
			System.out.println("Je m'appelle " 
					+ this.getNom().toUpperCase() 
					+ " et je ne suis pas employé d'une entreprise ");
		}
	}

	/**
	 * 
	 */
	public void quitterSociete() {
		/**
		 * déclarer la méthode quitterSociete en String pour 
		 * la version ternaire
		 */
		//		return societe == null 
		//			? societe = PAS_DE_SOCIETE 
		//			: getSociete();
		if(societe == null) {
			afficher();
			System.out.println("Je ne suis pas salarié: impossible de quitter la société");
			System.exit(1);
		}
		societe = PAS_DE_SOCIETE;
		System.out.println(this.getNom().toUpperCase()+ ":" + " J'ai quitté la société");
	}

	/**
	 * @param entreprise
	 * @return
	 */
	private String validerSociete(String entreprise) {
		if(societe.length() <= 30 && societe != PAS_DE_SOCIETE) {
			this.societe = entreprise;
		} else {
			System.out.println(getClass() + " " + nom.toUpperCase() + ", société incorrect: " + societe);
			System.exit(1);
		}
		return entreprise;
	}
}
